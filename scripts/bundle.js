// @ts-check

(async () => {
  // const path = await import('path');
  const esbuild = await import('esbuild');
  const fse = await import('fs-extra');

  await fse.emptyDir(`dist`);

  await esbuild.build({
    entryPoints: [`src/index.ts`],
    bundle: true,
    platform: `node`,
    format: `cjs`,
    // sourcemap: true,
    target: [`node16`],
    tsconfig: `tsconfig.json`,
    outdir: `dist`,
  });
})();
