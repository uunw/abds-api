// @ts-check

(async () => {
  // const util = await import('util');
  const esbuild = await import('esbuild');
  const fse = await import('fs-extra');
  // const cp = await import('child_process');
  const { exec } = await import('pkg');

  // const exec = util.promisify(cp.exec);

  await fse.emptyDir(`build`);
  await fse.emptyDir(`bin`);
  console.log(`clean dist dir, bin success`);

  await esbuild.build({
    entryPoints: [`src/index.ts`],
    bundle: true,
    minify: true,
    platform: `node`,
    format: `cjs`,
    target: [`node16`],
    tsconfig: `tsconfig.json`,
    outdir: `build`,
  });
  console.log(`build soruce file success`);

  // const { stdout, stderr } = await exec(
  //   'pkg -C GZip --options expose-gc --out-path bin ./build/index.js',
  // );

  // if (stderr) {
  //   console.error(stderr);
  //   process.exit(0);
  // }
  // console.log(stdout);
  // await exec([`./build/index.js`, `-C`, `GZip`, `--out-path`, `bin`]);
  // console.log(`build package ok`);
})();
