import { Schema, model, SchemaTypes } from 'mongoose';

interface IRole {
  name: string;
  rights: string[];
}

const schema = new Schema<IRole>(
  {
    name: { type: SchemaTypes.String, required: true },
    rights: { type: [SchemaTypes.String], required: false },
  },
  {
    timestamps: true,
    collection: `roles`,
  },
);

const RoleModel = model<IRole>(`Role`, schema);

export default RoleModel;
