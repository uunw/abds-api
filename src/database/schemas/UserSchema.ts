import { Schema, model, SchemaTypes } from 'mongoose';

interface IUser {
  username: string;
  name: string;
  password: Buffer;
  roles: string;
}

const schema = new Schema<IUser>(
  {
    username: { type: SchemaTypes.String, required: true },
    name: { type: SchemaTypes.String, required: true },
    password: { type: SchemaTypes.Buffer, required: true },
    roles: { type: SchemaTypes.String, required: false },
  },
  {
    timestamps: true,
    collection: `users`,
  },
);

const UserModel = model<IUser>(`User`, schema);

export default UserModel;
