import { Schema, model, SchemaTypes } from 'mongoose';

interface IRecord {
  name: string;
  studentsName: string;
  description: string;
  recordType: string;
}

const schema = new Schema<IRecord>(
  {
    name: { type: SchemaTypes.String },
    studentsName: { type: SchemaTypes.String },
    description: { type: SchemaTypes.String },
    recordType: { type: SchemaTypes.String, required: true },
  },
  {
    timestamps: true,
    collection: `records`,
  },
);

const RecordModel = model<IRecord>(`Record`, schema);

export default RecordModel;
