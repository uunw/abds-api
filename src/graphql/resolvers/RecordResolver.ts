import { ApolloError } from 'apollo-server';
import {
  Authorized,
  Resolver,
  Query,
  Mutation,
  ID,
  Arg,
  Args,
} from 'type-graphql';

import { RecordModel } from '../../database/schemas';
import {
  Record,
  AddRecordInput,
  UpdateRecordArgs,
  RemoveRecordArgs,
} from '../types/RecordType';
import { RoleType } from '../index';

@Resolver()
class RecordResolver {
  // @Authorized()
  @Query(() => [Record])
  async recentStudentRecords() {
    const records = await RecordModel.find().sort({ date: `desc` });

    return records;
  }

  @Query(() => [Record])
  async findRecordByType(@Arg(`recordType`, () => String) recordType: string) {
    const records = await RecordModel.find({
      recordType,
    });

    return records;
  }

  @Query(() => Record)
  async findRecordByID(@Arg(`id`, () => ID) id: string) {
    const record = await RecordModel.findById(id);

    return record;
  }

  // @Authorized([RoleType.ADMIN])
  @Mutation(() => ID)
  async addRecord(
    @Arg(`data`, () => AddRecordInput)
    { name, studentsName, description, recordType }: AddRecordInput,
  ) {
    const record = await RecordModel.create({
      name,
      studentsName,
      description,
      recordType,
    });

    return record.id;
  }

  // @Authorized([RoleType.ADMIN])
  @Mutation(() => ID!)
  async updateRecord(
    @Args(() => UpdateRecordArgs) { id, data }: UpdateRecordArgs,
  ) {
    const record = await RecordModel.findByIdAndUpdate(id, {
      ...data,
    });

    if (record.errors) throw new ApolloError(record.errors.message);

    return record.id;
  }

  // @Authorized([RoleType.SUPERVISOR])
  @Mutation(() => ID)
  async removeRecord(@Args(() => RemoveRecordArgs) { id }: RemoveRecordArgs) {
    const record = await RecordModel.findByIdAndRemove(id);

    if (record) return record.id;

    throw new ApolloError(`ไม่มีบันทึกนี้ในฐานข้อมูล`, `NO_RECORD`);
  }
}

export default RecordResolver;
