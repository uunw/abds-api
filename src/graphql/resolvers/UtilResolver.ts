import { Resolver, Query } from 'type-graphql';

@Resolver()
class UtilResolver {
  @Query(() => String)
  async ping() {
    return `ok`;
  }
}

export default UtilResolver;
