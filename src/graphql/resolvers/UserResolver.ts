import crypto from 'crypto';
import { ApolloError } from 'apollo-server';
import {
  Authorized,
  Resolver,
  Query,
  Mutation,
  Arg,
  ID,
  Args,
} from 'type-graphql';
import { isValidObjectId } from 'mongoose';
// import argon2 from 'argon2';

import { UserModel } from '../../database/schemas';
import {
  User,
  AddUserInput,
  RemoveUserArgs,
  UpdateUserArgs,
} from '../types/UserType';

import { hashPassword } from '../../utils';
import { RoleType } from '..';

@Resolver()
class UserResolver {
  @Authorized()
  @Query(() => User)
  async userProfile() {
    return { id: `asd` };
  }

  @Query(() => [User])
  async users() {
    const users = await UserModel.find();

    return users;
  }

  @Query(() => User)
  async findUserByID(@Arg(`id`, () => String) id: string) {
    const user = await UserModel.findById(id);

    return user;
  }

  @Mutation(() => ID)
  async addUser(
    @Arg(`newUser`, () => AddUserInput)
    { username, name, password }: AddUserInput,
  ): Promise<string> {
    const hasUser: boolean = await UserModel.exists({
      username,
    });

    // ถ้าไม่มีผู้ใช้งาน
    if (!hasUser) {
      const passHash = await hashPassword(Buffer.from(password, `base64`));

      const user = await UserModel.create({
        username,
        name,
        roles: RoleType.ADMIN,
        password: passHash,
      });

      return user.id;
    }

    throw new ApolloError(
      `ไม่สามารถเพิ่มผู้ใช้ได้เนื่องจากชื่อผู้ใช้เหมือนกับผู้ใช้อื่น`,
      `DUPLICATE_USERNAME`,
    );
  }

  @Mutation(() => ID)
  async updateUser(@Args(() => UpdateUserArgs) { id, data }: UpdateUserArgs) {
    const passHash = await hashPassword(Buffer.from(data.password, `base64`));

    const user = await UserModel.findByIdAndUpdate(id, {
      username: data.username,
      name: data.name,
      password: passHash,
    });

    if (!user) {
      throw new ApolloError(`ไม่มีบัญชีนี้ในระบบ`, `NO_USER`);
    }

    return user.id;
  }

  @Authorized([RoleType.SUPERVISOR])
  @Mutation(() => ID!)
  async removeUser(
    @Args(() => RemoveUserArgs) { id }: RemoveUserArgs,
  ): Promise<string> {
    if (isValidObjectId(id)) {
      const user = await UserModel.findByIdAndRemove(id);

      if (user) return user.id;

      throw new ApolloError(`ไม่มีผู้ใช้งานนี้ในระบบ`, `NO_USER_BY_ID`);
    }

    throw new ApolloError(`ไอดีของผู้ใช้งานไม่ถูกต้อง`, `INVALID_ID`);
  }
}

export default UserResolver;
