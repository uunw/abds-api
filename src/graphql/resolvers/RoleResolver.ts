import { Resolver, Mutation, Args, ID } from 'type-graphql';

import { RoleModel } from '../../database/schemas';
import { AddRoleArgs } from '../types/RoleType';

@Resolver()
class RoleReslover {
  @Mutation(() => ID)
  async newRole(@Args(() => AddRoleArgs) { name }: AddRoleArgs) {
    const role = await RoleModel.create({
      name,
    });

    return role.id;
  }
}

export default RoleReslover;
