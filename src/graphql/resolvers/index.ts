import UserResolver from './UserResolver';
import AuthResolver from './AuthResolver';
import RoleReslover from './RoleResolver';
import RecordResolver from './RecordResolver';

import UtilResolver from './UtilResolver';

export {
  UserResolver,
  AuthResolver,
  RoleReslover,
  RecordResolver,
  UtilResolver,
};
