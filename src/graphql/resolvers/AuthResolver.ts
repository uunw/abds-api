import { ApolloError } from 'apollo-server';
import { Resolver, Mutation, Args } from 'type-graphql';
import { SignJWT } from 'jose';
import { v5 as uuidv5 } from 'uuid';

import { UserModel } from '../../database/schemas';
import { LoginArgs, LoginResult } from '../types/AuthType';
import { verifyPassword } from '../../utils';
// import argon2 from 'argon2';

@Resolver()
class AuthResolver {
  @Mutation(() => LoginResult)
  async login(@Args(() => LoginArgs) { username, password }: LoginArgs) {
    const user = await UserModel.findOne({ username });

    if (user) {
      const isPasswordCorrect = await verifyPassword(
        Buffer.from(password, `base64`),
        user.password,
      );

      // เมื่ออีเมลและรหัสผ่านถูกต้อง
      if (isPasswordCorrect) {
        const signKey = Buffer.from(
          process.env.JWT_SECRET || `aa0987`,
          `base64`,
        );

        const token = await new SignJWT({
          sub: user.id,
          username: user.username,
          roles: user.roles,
          jti: uuidv5(Buffer.from(user.id, `base64`), uuidv5.URL).toString(),
        })
          .setProtectedHeader({ typ: `JWT`, alg: `HS512` })
          .setIssuedAt(Date.now())
          .setIssuer('abds.cmtc.ac.th')
          .setAudience(user.name)
          .setExpirationTime(Date.now() + 20 * (1000 * 60))
          .sign(signKey);

        const result = {
          token: token,
          user,
        };

        return result;
      }

      throw new ApolloError(`รหัสผ่านไม่ถูกต้อง`, `PASSWORD_NOT_CORRECT`);
    }

    throw new ApolloError(`ชื่อผู้ใช้ไม่ถูกต้อง`, `WRONG_USERNAME`);
  }
}

export default AuthResolver;
