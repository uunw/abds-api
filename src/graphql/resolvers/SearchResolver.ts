import { Resolver, Query } from 'type-graphql';

@Resolver()
class SearchResolver {
  @Query(() => String)
  async simpleSearch() {
    return 'OK !!';
  }
}

export default SearchResolver;
