import {
  ObjectType,
  InterfaceType,
  InputType,
  Field,
  ID,
  ArgsType,
} from 'type-graphql';
import { Length, MaxLength, IsArray, IsString, IsEnum } from 'class-validator';

enum RecordType {
  ADMONISH = `ADMONISH`,
  PKS_SCORE = `PKS_SCORE`,
  PAROLE = `PAROLE`,
  ACTIVITY = `ACTIVITY`,
}

@InterfaceType()
class IRecord {
  @Field(() => String)
  name: string;

  @Field(() => String)
  studentsName: string;

  @Field(() => String)
  description: string;

  @Field(() => String)
  recordType: string;
}

@ObjectType({ implements: IRecord })
class Record implements IRecord {
  name: string;
  studentsName: string;
  description: string;
  recordType: RecordType;

  @Field(() => ID)
  id: string;

  @Field(() => Date)
  createdAt: Date;

  @Field(() => Date)
  updatedAt: Date;
}

@InputType()
class AddRecordInput {
  @Field(() => String)
  @Length(1, 40)
  name: string;

  @Field(() => String)
  studentsName: string;

  @Field(() => String)
  @MaxLength(1024)
  description: string;

  @Field(() => String)
  recordType: string;
}

@InputType()
class UpdateRecordDataInput {
  @Field(() => String, { nullable: true })
  @MaxLength(40)
  name: string;

  @Field(() => String, { nullable: true })
  studentsName: string;

  @Field(() => String, { nullable: true })
  @MaxLength(1024)
  description: string;
}

@ArgsType()
class UpdateRecordArgs {
  @Field(() => ID)
  @Length(24)
  id: string;

  @Field(() => UpdateRecordDataInput!)
  data: UpdateRecordDataInput;
}

@ArgsType()
class RemoveRecordArgs {
  @Field(() => ID)
  @IsString()
  id: string;
}

export {
  Record,
  AddRecordInput,
  UpdateRecordArgs,
  RemoveRecordArgs,
  RecordType,
};
