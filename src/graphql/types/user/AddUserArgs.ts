import { ArgsType, Field } from 'type-graphql';
import { Length, MaxLength } from 'class-validator';

@ArgsType()
class AddUserArgs {
  @Field(() => String)
  @MaxLength(40)
  username: string;

  @Field(() => String)
  @Length(4, 50)
  name: string;

  @Field(() => String)
  @Length(8, 255)
  password: string;
}

export default AddUserArgs;
