import { ObjectType, ArgsType, Field } from 'type-graphql';
import { Length } from 'class-validator';

import { User } from './UserType';

@ObjectType()
class LoginResult {
  @Field(() => String)
  token: string;

  @Field(() => User)
  user: User;
}

@ArgsType()
class LoginArgs {
  @Field(() => String)
  @Length(5, 20)
  username: string;

  @Field(() => String)
  @Length(8, 255)
  password: string;
}

export { LoginArgs, LoginResult };
