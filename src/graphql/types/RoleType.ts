import { ObjectType, InterfaceType, Field, ArgsType } from 'type-graphql';
import { Length } from 'class-validator';

@InterfaceType()
class IRole {
  @Field(() => String)
  name: string;

  @Field(() => [String])
  rights: string[];
}

@ObjectType({ implements: IRole })
class Role implements IRole {
  name: string;
  rights: string[];
}

@ArgsType()
class AddRoleArgs {
  @Field(() => String)
  @Length(1, 20)
  name: string;

  @Field(() => [String])
  rights: string[];
}

export { IRole, Role, AddRoleArgs };
