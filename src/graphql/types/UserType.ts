import {
  InterfaceType,
  ObjectType,
  InputType,
  Field,
  ID,
  ArgsType,
  Authorized,
} from 'type-graphql';
import { Length, MaxLength } from 'class-validator';

@InterfaceType()
class IUser {
  @Field(() => ID)
  id: string;

  @Field(() => String)
  username: string;

  @Field(() => String)
  name: string;

  @Field(() => String)
  roles: string;

  @Field(() => Date)
  createdAt: Date;

  @Field(() => Date)
  updatedAt: Date;
}

@ObjectType({ implements: IUser })
class User implements IUser {
  id: string;
  username: string;
  name: string;
  roles: string;
  createdAt: Date;
  updatedAt: Date;
}

@ArgsType()
class RemoveUserArgs {
  @Field(() => ID)
  @MaxLength(24)
  id: string;
}

@InputType({ description: `asd` })
class AddUserInput {
  @Field(() => String)
  @Length(5, 20)
  username: string;

  @Field(() => String)
  @Length(4, 40)
  name: string;

  @Field(() => String)
  @Length(8, 255)
  password: string;

  @Field(() => String, { nullable: true, defaultValue: `ADMIN` })
  roles: string;
}

@ArgsType()
class UpdateUserArgs {
  @Field(() => ID)
  id: string;

  @Field(() => UpdateUserDataInput)
  data: UpdateUserDataInput;
}

@InputType({ description: `เปลี่ยนข้อมูลผู้ใช้งาน` })
class UpdateUserDataInput {
  @Field(() => String, { nullable: true })
  @Length(5, 20)
  username: string;

  @Field(() => String, { nullable: true })
  @Length(4, 40)
  name: string;

  @Field(() => String, { nullable: true })
  @Length(8, 255)
  password: string;
}

export {
  IUser,
  User,
  RemoveUserArgs,
  AddUserInput,
  UpdateUserArgs,
  UpdateUserDataInput,
};
