import { ApolloError } from 'apollo-server';
import { AuthChecker } from 'type-graphql';
import { jwtVerify } from 'jose';

import { UserModel } from '../database/schemas';

interface ContextType {
  token: string;
}

enum RoleType {
  SUPERVISOR = `SUPERVISOR`,
  ADMIN = `ADMIN`,
}

/**
 * Remove Bearer from auth header
 * @param token String
 */
async function parseToken(token: string): Promise<string | null> {
  const bearer: string = `Bearer `;

  return new Promise((resolver, reject) => {
    if (token === null) reject(null);

    if (token.startsWith(bearer)) {
      return resolver(token.substring(bearer.length, token.length));
    }

    reject(null);
  });
}

const authChecker: AuthChecker<ContextType, RoleType> = async (
  { context },
  roles,
) => {
  const JWT_SECRET = Buffer.from(process.env.JWT_SECRET || `aa0987`, `base64`);

  const token = await parseToken(context.token);
  // console.log(token.replace(/["]+/g, ''));

  if (!!token) {
    const { payload } = await jwtVerify(token.replace(/["]+/g, ''), JWT_SECRET);

    // console.log(Date.now());
    // console.log(payload.exp);

    if (Date.now() <= payload.exp) {
      const user = await UserModel.findById(payload.sub);

      if (!user)
        throw new ApolloError(`ไม่มีบัญชีผู้ใช้นี้ในระบบ`, `USER_NOT_FOUND`);

      // if(payload.roles.)

      // console.log(user);
      // console.log(roles);
      // console.log(payload?.roles);
      // console.log(!!payload);

      return true;
    }

    // !!roles && console.log(roles);

    throw new ApolloError(`token หมดอายุ`, `TOKEN_EXPIRED`);
  }

  return false;
};

export type { ContextType };
export { authChecker, RoleType };
