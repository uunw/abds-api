import crypto from 'crypto';

interface IConfig {
  hashBytes: number;
  saltBytes: number;
  iterations: number;
  digest: `sha512`;
}

const config: IConfig = {
  hashBytes: 128,
  saltBytes: 512,
  iterations: 872791,
  digest: `sha512`,
};

function pbkdf2(
  value: Buffer,
  salt: crypto.BinaryLike,
  iterations: number,
  hashBytes: number,
  digest: string,
): Promise<Buffer> {
  return new Promise((resolve, reject) => {
    crypto.pbkdf2(value, salt, iterations, hashBytes, digest, (err, key) => {
      if (err) return reject(err);

      resolve(key);
    });
  });
}

async function hashPassword(
  password: Buffer,
  hashSalt?: Buffer,
): Promise<Buffer> {
  const salt = hashSalt ? hashSalt : crypto.randomBytes(config.hashBytes);

  const hash = await pbkdf2(
    password,
    salt,
    config.iterations,
    config.hashBytes,
    config.digest,
  );
  const combined = Buffer.alloc(hash.length + salt.length + 8);

  combined.writeUInt32BE(salt.length, 0);
  combined.writeUInt32BE(config.iterations, 4);

  salt.copy(combined, 8);
  hash.copy(combined, salt.length + 8);

  return combined;
}

async function verifyPassword(
  password: Buffer,
  combined: Buffer,
): Promise<boolean> {
  const saltBytes = combined.readUInt32BE(0);
  const hashBytes = combined.length - saltBytes - 8;
  const iterations = combined.readUInt32BE(4);

  const salt = combined.slice(8, saltBytes + 8);
  const hash = combined.toString(`binary`, saltBytes + 8);

  const pass = await pbkdf2(
    password,
    salt,
    iterations,
    hashBytes,
    config.digest,
  );

  return pass.toString(`binary`) === hash;
}

export { hashPassword, verifyPassword };
