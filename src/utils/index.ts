import { UserModel } from '../database/schemas';
import { RoleType } from '../graphql';

import { hashPassword, verifyPassword } from './hash';

async function insertDefaultSupervisorUser() {
  const username = String(process.env.MONGO_USERNAME);
  const password = String(process.env.MONGO_PASSWORD);

  const user = await UserModel.findOne({
    username,
  });

  if (!user) {
    const passHash = await hashPassword(Buffer.from(password, `base64`));

    const data = await UserModel.create({
      username,
      name: `default ${username} user`,
      roles: RoleType.SUPERVISOR,
      password: passHash,
    });

    console.log(data.username);
  }
}

export { hashPassword, verifyPassword, insertDefaultSupervisorUser };
