import 'reflect-metadata';
import 'dotenv/config';

/**
 * Main Program
 */
(async () => {
  // using dynamic import
  const { ApolloServer } = await import('apollo-server');
  const { ApolloServerPluginInlineTrace } = await import('apollo-server-core');
  const { buildSchema, registerEnumType } = await import('type-graphql');
  const mongoose = await import('mongoose');

  const { authChecker } = await import('./graphql');
  const {
    AuthResolver,
    RoleReslover,
    UserResolver,
    RecordResolver,
    UtilResolver,
  } = await import('./graphql/resolvers');

  const { RecordType } = await import('./graphql/types/RecordType');

  const { insertDefaultSupervisorUser } = await import('./utils');

  const MONGO_URI = String(
    process.env.MONGO_URI || `mongodb://localhost:27017/abds`,
  );
  const MONGO_USERNAME = process.env.MONGO_USERNAME;
  const MONGO_PASSWORD = process.env.MONGO_PASSWORD;

  const PORT: number = parseInt(process.env.APP_PORT || `3090`, 10);

  const schema = await buildSchema({
    authChecker,
    authMode: process.env.NODE_ENV !== `production` ? `error` : `null`,
    resolvers: [
      UserResolver,
      AuthResolver,
      RoleReslover,
      RecordResolver,
      UtilResolver,
    ],
    dateScalarMode: `timestamp`,
  });

  registerEnumType(RecordType, {
    name: `RecordType`,
  });

  const server = new ApolloServer({
    schema,
    context: async ({ req }) => {
      const token: string | null = req.headers
        ? req.headers[`authorization`] || null
        : null;

      return { token };
    },
    plugins: [ApolloServerPluginInlineTrace()],
    formatError: (err) => {
      return err;
    },
  });

  if (MONGO_URI) {
    try {
      await mongoose
        .connect(MONGO_URI, {
          auth: {
            username: MONGO_USERNAME,
            password: MONGO_PASSWORD,
          },
          w: `majority`,
          retryWrites: true,
        })
        .then(() => {
          console.log(`mongo ${MONGO_URI} is connected!`);
        });

      await insertDefaultSupervisorUser();

      if (PORT) {
        try {
          await server.listen(PORT, () => {
            console.log(
              `server listen on: http://localhost:${PORT}${server.graphqlPath}`,
            );
          });
        } catch (err) {
          console.log(`can't start graphql server!`);
        }
      }
    } catch (err) {
      console.log(`can't connect database!`);
    }
  }
})();
